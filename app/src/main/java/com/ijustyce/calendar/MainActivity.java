package com.ijustyce.calendar;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.LayoutInflater;

import com.ijustyce.calendar.bean.DateBean;
import com.ijustyce.calendar.databinding.DayView;
import com.ijustyce.calendar.databinding.DetailDateView;
import com.ijustyce.calendar.databinding.MainView;
import com.ijustyce.calendar.model.DateModel;
import com.ijustyce.calendar.utils.LunarCalendar;
import com.ijustyce.calendar.utils.LunarYearDetail;
import com.ijustyce.fastandroiddev3.base.BaseListActivity;
import com.ijustyce.fastandroiddev3.baseLib.utils.DateUtil;
import com.ijustyce.fastandroiddev3.baseLib.utils.StringUtils;
import com.ijustyce.fastandroiddev3.irecyclerview.BindingInfo;
import com.ijustyce.fastandroiddev3.irecyclerview.IRecyclerView;

import java.util.ArrayList;

import retrofit2.Call;

public class MainActivity extends BaseListActivity<MainView, DateBean, DateModel> {

    private String currentDay;
    @Override
    public void afterCreate() {
        String date = DateUtil.getDateString("yyyy-MM-dd");
        contentView.day.setText(date.substring(5));

        String month = date.substring(5, 7);
        String day = date.substring(8);
        String year = date.substring(0, 4);
        if (month.startsWith("0")) month = month.substring(1);
        if (day.startsWith("0")) day = day.substring(1);
        int yearInt = StringUtils.getInt(year);

        int nongli[] = LunarCalendar.solarToLunar(yearInt,
                StringUtils.getInt(month), StringUtils.getInt(day));
        if (nongli[1] == 11) month = "冬";
        else if (nongli[1] == 12) month = "腊";
        else month = StringUtils.arabicNumberToChinese(nongli[1]);
        if (nongli[3] == 1) month = "闰" + month;
        month += "月";
        day = StringUtils.arabicNumberToChinese(nongli[2]);
        if (nongli[2] < 11) day = "初" + day;

        DetailDateView detailDateView = DataBindingUtil.inflate(
                LayoutInflater.from(context), R.layout.view_calendar_end, null, false);
        detailDateView.detail.setText(LunarYearDetail.getDetail(nongli[0]) + month + day);

        contentView.list.addFooterView(detailDateView.getRoot());
        contentView.list.setGirdLayout(7);
        contentView.list.showFooterLabel(false);
        contentView.list.setCanRefresh(false);
    }

    private DateModel buildModel(){
        DateModel dateModel = new DateModel();
        dateModel.dateBeen = new ArrayList<>();
        for (int i = 1; i < 8; i++) {
            DateBean dateBean = new DateBean();
            dateBean.week = StringUtils.arabicNumberToChinese(i);
            dateModel.dateBeen.add(dateBean);
        }
        for (int i = 1; i < 32; i++) {
            DateBean dateBean = new DateBean();
            dateBean.date = i + "";
            int nongli = LunarCalendar.solarToLunar(2017, 1, i)[2];
            dateBean.sub = StringUtils.arabicNumberToChinese(nongli);
            if (nongli > 20 && nongli < 30) {
                dateBean.sub = dateBean.sub.replace("二十", "廿");
            }if (nongli < 11) {
                dateBean.sub = "初" + dateBean.sub;
            }
            dateModel.dateBeen.add(dateBean);
        }
        return dateModel;
    }

    @Override
    public BindingInfo getBindingInfo() {
        BindingInfo bindingInfo = BindingInfo.createByLayoutIdAndBindName(R.layout.item_day, BR.DateModel);
        currentDay = DateUtil.getDateString("dd");
        if (currentDay != null && currentDay.startsWith("0")) {
            currentDay = currentDay.substring(1);
        }
        bindingInfo.setAfterCreateCallBack(new BindingInfo.ItemCreate() {
            @Override
            public <T> void onCreated(T item, int position, ViewDataBinding binding) {
                DayView dayView = binding instanceof DayView ? (DayView) binding : null;
                DateBean dateModel = item instanceof DateBean ? (DateBean) item : null;
                if (dayView == null || dateModel == null) return;
                if (currentDay.equals(dateModel.date)) {
                    dayView.item.setBackground(getDrawable(R.drawable.bg_current_day));
                    dayView.day.setTextColor(getResColor(R.color.white));
                    dayView.sub.setTextColor(getResColor(R.color.white));
                }else {
                    dayView.item.setBackground(getDrawable(R.drawable.day_view_normal));
                    dayView.day.setTextColor(getResColor(R.color.black2));
                    dayView.sub.setTextColor(getResColor(R.color.gray));
                }
            }
        });
        return bindingInfo;
    }

    @Override
    public IRecyclerView getRecyclerView() {
        return contentView.list;
    }

    @Override
    public Call<DateModel> getListCall(int pageNo) {
        return null;
    }

    @Override
    public void onDataShow(ArrayList<DateBean> data) {
        DateModel dateModel = buildModel();
        contentView.list.dataInterface.bindDataToView(dateModel);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }
}
