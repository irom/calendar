package com.ijustyce.calendar.model;

import com.ijustyce.calendar.bean.DateBean;
import com.ijustyce.fastandroiddev3.irecyclerview.IResponseData;

import java.util.ArrayList;

/**
 * Created by yangchun on 2017/1/15.
 */

public class DateModel extends IResponseData<DateBean> {

    public ArrayList<DateBean> dateBeen;
    @Override
    public ArrayList<DateBean> getList() {
        return dateBeen;
    }
}
